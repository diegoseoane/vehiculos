const autos = [];
const agregar = document.querySelector('#agregar');
const shift = document.querySelector('#shift');
const pop = document.querySelector('#pop');
const auto = document.querySelector('input[type="text"]');
const lista = document.querySelector('#lista');

// muestra la lista de autos adquiridos
function list(){
    lista.innerHTML = "";
    for(item of autos){
        lista.innerHTML += item + "<br>";
    }
};
// si el campo no está vacío, agrega el auto al array y muestra la lista
agregar.addEventListener('click', function(e) {
    if (auto.value == "") {
        auto.classList.add("error");
    } else {
        autos.push(auto.value);
        auto.value = "";
        list();
    } 
});
// elimina primer auto y muestra lista
shift.addEventListener('click', function(e) {
    autos.shift();
    list();
});
// elimina último auto y muestra lista
pop.addEventListener('click', function(e) {
    autos.pop();
    list();
});